import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PrimaryNavigationComponent } from './primary-navigation.component';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

describe('PrimaryNavigationComponent', () => {
  let component: PrimaryNavigationComponent;
  let fixture: ComponentFixture<PrimaryNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimaryNavigationComponent ],
      imports: [ RouterTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimaryNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create a primary navigation', () => {
    expect(component).toBeTruthy();
  });

  it('should have a list of items if we pass them', () => {
    const items = [
      { name: 'Foo', route: '/foo' },
      { name: 'Bar', route: '/bar' }
    ];
    component.items = items;
    expect(component.items).toEqual(items);
  });

  it('should have a toggle value of false and a class of hide', () => {
    const nav = fixture.debugElement.query(By.css('.nav-list.hide'));
    expect(component.toggle).toBeFalsy();
    expect(nav).toBeTruthy();
  });

  it('should have a toggle value of true and a class of show when toggled', () => {
    const toggleButton = fixture.debugElement.query(By.css('.nav-toggle'));
    toggleButton.triggerEventHandler('click', {});
    fixture.detectChanges();

    const nav = fixture.debugElement.query(By.css('.nav-list.show'));
    expect(component.toggle).toBeTruthy();
    expect(nav).toBeTruthy();
  });
});
