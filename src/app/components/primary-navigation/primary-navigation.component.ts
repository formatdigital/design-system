import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-primary-navigation',
  templateUrl: './primary-navigation.component.html',
  styleUrls: ['./primary-navigation.component.scss']
})

export class PrimaryNavigationComponent {
  @Input() items: Array<any>;
  toggle: boolean;

  onToggle() {
    this.toggle = !this.toggle;
  }
}
