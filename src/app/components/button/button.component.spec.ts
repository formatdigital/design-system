import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ButtonComponent } from './button.component';

describe('ButtonComponent', () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create a button', () => {
    expect(component).toBeTruthy();
  });

  it('should have a type of button by default', () => {
    expect(component.type).toBe('button');
  });

  it('should have a css class of btn by default', () => {
    expect(component.cssClass).toBe('btn');
  });

  it('should have a type of submit if we pass that type', () => {
    component.type = 'submit';
    expect(component.type).toBe('submit');
  });

  it('should have css classes of "btn primary" if we pass primary', () => {
    component.cssClass = 'primary';
    component.ngOnInit();
    expect(component.cssClass).toBe('btn primary');
  });
});
