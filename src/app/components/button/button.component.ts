import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})

export class ButtonComponent implements OnInit {
  @Input() type = 'button';
  @Input() cssClass: string;
  @Output() clicked = new EventEmitter<MouseEvent>();

  onClickButton(event) {
    this.clicked.emit(event);
  }

  ngOnInit() {
    this.cssClass = !this.cssClass ? 'btn' : `btn ${this.cssClass}`;
  }
}
