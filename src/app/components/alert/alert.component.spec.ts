import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AlertComponent } from './alert.component';
import { By } from '@angular/platform-browser';

describe('AlertComponent', () => {
  let component: AlertComponent;
  let fixture: ComponentFixture<AlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create an alert component', () => {
    expect(component).toBeTruthy();
  });

  it('should have a status of alert by default', () => {
    expect(component.status).toBe('alert');
  });

  it('should have css classes of "alert warning" if we pass a warning status', () => {
    component.status = 'warning';
    component.ngOnInit();
    expect(component.status).toBe('alert warning');
  });

  it('should not be a sticky alert if not set', () => {
    const sticky = fixture.debugElement.query(By.css('.sticky'));
    expect(sticky).toBeFalsy();
  });

  it('should be a sticky alert if set', () => {
    component.sticky = true;
    fixture.detectChanges();
    const sticky = fixture.debugElement.query(By.css('.sticky'));
    expect(sticky).toBeTruthy();
  });
});
