import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { PhaseBannerComponent } from './phase-banner.component';

describe('PhaseBannerComponent', () => {
  let component: PhaseBannerComponent;
  let fixture: ComponentFixture<PhaseBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhaseBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhaseBannerComponent);
    component = fixture.componentInstance;
    component.phase = 'alpha';
    fixture.detectChanges();
  });

  it('should create a phase banner', () => {
    expect(component).toBeTruthy();
  });

  it('should render a phase banner if it has a phase', () => {
    const banner = fixture.debugElement.query(By.css('.phase-banner'));
    expect(banner).toBeTruthy();
  });
});
