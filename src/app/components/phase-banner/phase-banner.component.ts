import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-phase-banner',
  templateUrl: './phase-banner.component.html',
  styleUrls: ['./phase-banner.component.scss']
})
export class PhaseBannerComponent {
  @Input() phase: string;
  @Input() feedbackUrl: string;
}
