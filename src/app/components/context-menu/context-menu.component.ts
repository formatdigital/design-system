import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-context-menu',
  templateUrl: './context-menu.component.html',
  styleUrls: ['./context-menu.component.scss']
})

export class ContextMenuComponent implements OnInit {
  constructor(public route: ActivatedRoute) {}
  @Input() outlet: string;
  @Input() items: Array<any>;
  activeFragment: string;

  ngOnInit() {
    this.activeFragment = 'unset';
    this.route.fragment.subscribe((fragment: string) => fragment ? this.activeFragment = fragment : null);
    if (this.items) {
      this.items.map(item => {
        if (this.outlet) {
          item.route = [`/${this.outlet}`, { outlets: {[this.outlet] : [item.route] } }];
        } else {
          item.route = item.route ? item.route : './';
        }
      });
    }
  }
}
