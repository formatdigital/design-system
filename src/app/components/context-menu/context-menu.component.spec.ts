import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ContextMenuComponent } from './context-menu.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('ContextMenuComponent', () => {
  let component: ContextMenuComponent;
  let fixture: ComponentFixture<ContextMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContextMenuComponent ],
      imports: [ RouterTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create a context menu', () => {
    expect(component).toBeTruthy();
  });

  it('should have a list of items if we pass them', () => {
    const items = [
      { name: 'Foo', route: 'foo' },
      { name: 'Bar', route: 'bar' }
    ];
    component.items = items;
    expect(component.items).toEqual(items);
  });

  it('should have a list of items with a default route of "./" to allow routing to page fragments', () => {
    const items = [
      { name: 'Foo', fragment: 'foo' },
      { name: 'Bar', fragment: 'bar' }
    ];
    component.items = items;
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.items).toEqual([
      { name: 'Foo', route: './', fragment: 'foo' },
      { name: 'Bar', route: './', fragment: 'bar' }]
    );
  });

  it('should allow routing to a named outlet if specified', () => {
    const items = [
      { name: 'Foo', route: 'foo' },
      { name: 'Bar', route: 'bar' }
    ];
    component.items = items;
    component.outlet = 'foobars';
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.items).toEqual([
      { name: 'Foo', route: [ '/foobars', Object({ outlets: Object({ foobars: [ 'foo' ] }) }) ] },
      { name: 'Bar', route: [ '/foobars', Object({ outlets: Object({ foobars: [ 'bar' ] }) }) ] }]
    );
  });
});
