import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { LoadingIndicatorComponent } from './loading-indicator.component';

describe('LoadingIndicatorComponent', () => {
  let component: LoadingIndicatorComponent;
  let fixture: ComponentFixture<LoadingIndicatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingIndicatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create a loading indicator', () => {
    expect(component).toBeTruthy();
  });

  it('should not render a loading indicator if "loading" not set to true', () => {
    const indicator = fixture.debugElement.query(By.css('.loading'));
    expect(indicator).toBeFalsy();
  });

  it('should render a loading indicator if "loading" is set to true', () => {
    component.loading = true;
    fixture.detectChanges();

    const indicator = fixture.debugElement.query(By.css('.loading')).nativeElement;
    expect(indicator).toBeTruthy();
    expect(indicator.textContent).toBe('Loading...');
  });
});
