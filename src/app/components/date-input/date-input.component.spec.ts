import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DateInputComponent } from './date-input.component';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';

describe('DateInputComponent', () => {
  let component: DateInputComponent;
  let fixture: ComponentFixture<DateInputComponent>;
  const formGroup = new FormGroup({
    day: new FormControl(),
    month: new FormControl(),
    year: new FormControl()
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DateInputComponent
      ],
      imports: [ ReactiveFormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateInputComponent);
    component = fixture.componentInstance;
    component.formGroup = formGroup;
    fixture.detectChanges();
  });

  it('should create a date input if there is a form group', () => {
    expect(component).toBeTruthy();
  });

  it('should have a day label', () => {
    const label = fixture.debugElement.query(By.css('label[for=day]')).nativeElement;
    expect(label.textContent).toBe('Day');
  });

  it('should have a day input', () => {
    const input = fixture.debugElement.query(By.css('input#day')).nativeElement;
    expect(input).toBeTruthy();
  });

  it('should have a month label', () => {
    const label = fixture.debugElement.query(By.css('label[for=month]')).nativeElement;
    expect(label.textContent).toBe('Month');
  });

  it('should have a month input', () => {
    const input = fixture.debugElement.query(By.css('input#month')).nativeElement;
    expect(input).toBeTruthy();
  });

  it('should have a year label', () => {
    const label = fixture.debugElement.query(By.css('label[for=year]')).nativeElement;
    expect(label.textContent).toBe('Year');
  });

  it('should have a year input', () => {
    const input = fixture.debugElement.query(By.css('input#year')).nativeElement;
    expect(input).toBeTruthy();
  });
});
