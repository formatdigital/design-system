import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-date-input',
  templateUrl: './date-input.component.html',
  styleUrls: ['./date-input.component.scss']
})

export class DateInputComponent implements OnInit {
  @Input() cssClass: string;
  @Input() formGroup: FormGroup;

  limit(element, chars = 2) {
    if (element.target.value.length > chars) {
        element.target.value = element.target.value.substr(0, chars);
    }
  }

  ngOnInit() {
    !this.cssClass ? this.cssClass = 'date-input' : this.cssClass = `date-input ${this.cssClass}`;
  }
}
