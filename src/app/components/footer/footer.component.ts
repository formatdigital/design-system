import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})

export class FooterComponent implements OnInit {
  generalItems: Array<any>;
  legalItems: Array<any>;

  ngOnInit() {
    this.generalItems = [
      { name: 'Laboratory opertations', route: '/' },
      { name: 'Accreditations and certifications', route: '/' }
    ];

    this.legalItems = [
      { name: 'Privacy policy', route: '/' },
      { name: 'Terms of use', route: '/' },
      { name: 'Contact', route: '/' }
    ];
  }
}
