import { Component, ContentChildren, QueryList, AfterContentInit } from '@angular/core';
import { TabComponent as Tab } from './tab.component';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})

export class TabsComponent implements AfterContentInit {
  @ContentChildren(Tab) tabs: QueryList<Tab>;
  selectTab(tab: Tab) {
    this.tabs.toArray().forEach(item => item.active = false);
    tab.active = true;
  }

  ngAfterContentInit() {
    if (this.tabs.length) {
      const activeTabs = this.tabs.filter(tab => tab.active);

      if (activeTabs.length === 0) {
        this.selectTab(this.tabs.first);
      }
    }
  }
}
