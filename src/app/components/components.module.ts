import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HeaderComponent } from './header/header.component';
import { PrimaryNavigationComponent } from './primary-navigation/primary-navigation.component';
import { ButtonComponent } from './button/button.component';
import { ContextMenuComponent } from './context-menu/context-menu.component';
import { FooterComponent } from './footer/footer.component';
import { FooterNavigationComponent } from './footer-navigation/footer-navigation.component';
import { PhaseBannerComponent } from './phase-banner/phase-banner.component';
import { DateInputComponent } from './date-input/date-input.component';
import { TabsComponent } from './tabs/tabs.component';
import { TabComponent } from './tabs/tab.component';
import { SegmentedControlComponent } from './segmented-control/segmented-control.component';
import { LoadingIndicatorComponent } from './loading-indicator/loading-indicator.component';
import { AlertComponent } from './alert/alert.component';

@NgModule({
  declarations: [
    AlertComponent,
    HeaderComponent,
    FooterComponent,
    PrimaryNavigationComponent,
    ButtonComponent,
    ContextMenuComponent,
    FooterNavigationComponent,
    PhaseBannerComponent,
    DateInputComponent,
    TabsComponent,
    TabComponent,
    SegmentedControlComponent,
    LoadingIndicatorComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    AlertComponent,
    HeaderComponent,
    FooterComponent,
    PrimaryNavigationComponent,
    ButtonComponent,
    ContextMenuComponent,
    FooterNavigationComponent,
    PhaseBannerComponent,
    DateInputComponent,
    TabsComponent,
    TabComponent,
    SegmentedControlComponent,
    LoadingIndicatorComponent
  ]
})

export class ComponentsModule { }
