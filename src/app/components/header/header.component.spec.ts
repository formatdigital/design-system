import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { PrimaryNavigationComponent } from '../primary-navigation/primary-navigation.component';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HeaderComponent,
        PrimaryNavigationComponent
      ],
      imports: [ RouterTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create a header', () => {
    expect(component).toBeTruthy();
  });

  it('should not be a sticky header if not set', () => {
    const sticky = fixture.debugElement.query(By.css('.sticky'));
    expect(sticky).toBeFalsy();
  });

  it('should be a sticky header if set', () => {
    component.sticky = true;
    fixture.detectChanges();
    const sticky = fixture.debugElement.query(By.css('.sticky'));
    expect(sticky).toBeTruthy();
  });

  it('should have an app heading', () => {
    const heading = fixture.debugElement.query(By.css('.app-heading'));
    expect(heading).toBeTruthy();
  });

  it('should not render a primary navigation if not set', () => {
    const nav = fixture.debugElement.query(By.css('.primary-navigation'));
    expect(nav).toBeFalsy();
  });

  it('should have a primary navigation if set', () => {
    component.navItems = [
      { name: 'Home', route: '/home' },
      { name: 'Styles', route: '/styles' },
      { name: 'Components', route: '/components' },
      { name: 'Patterns', route: '/patterns' }
    ];
    fixture.detectChanges();

    const nav = fixture.debugElement.query(By.css('.primary-navigation'));
    expect(nav).toBeTruthy();
  });
});
