import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'app-segmented-control',
  templateUrl: './segmented-control.component.html',
  styleUrls: ['./segmented-control.component.scss']
})

export class SegmentedControlComponent implements OnInit {
  @Input() name: string;
  @Input() segments: Array<object>;
  @Input() cssClass: string;
  @Output() changed = new EventEmitter<string>();

  handleClick(value: string) {
    this.changed.emit(value);
  }

  ngOnInit() {
    this.cssClass = !this.cssClass ? 'segmented-control' : `segmented-control ${this.cssClass}`;
  }
}
