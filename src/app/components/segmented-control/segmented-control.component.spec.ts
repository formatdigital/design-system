import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { SegmentedControlComponent } from './segmented-control.component';

describe('SegmentedControlComponent', () => {
  let component: SegmentedControlComponent;
  let fixture: ComponentFixture<SegmentedControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SegmentedControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentedControlComponent);
    component = fixture.componentInstance;
    component.name = 'foo-bar';
    fixture.detectChanges();
  });

  it('should create a segmented control', () => {
    expect(component).toBeTruthy();
  });

  let segments;
  beforeEach(() => {
    segments = [
      { id: 'foo', value: 'foo', label: 'Foo', checked: true },
      { id: 'bar', value: 'bar', label: 'Bar' }
    ];
    component.segments = segments;
    fixture.detectChanges();
  });

  it('should have a list of segments if we pass them', () => {
    expect(component.segments).toEqual(segments);
  });

  it('should have an input with an ID passed in segments', () => {
    const input = fixture.debugElement.query(By.css('input#foo'));
    expect(input).toBeTruthy();
  });

  it('should have an input with the value passed in segments', () => {
    const input = fixture.debugElement.query(By.css('input#foo')).nativeElement;
    expect(input.value).toBe('foo');
  });

  it('should have a label with the text passed in segments', () => {
    const input = fixture.debugElement.query(By.css('label[for=foo]')).nativeElement;
    expect(input.textContent).toBe('Foo');
  });

  it('should have a css class of segmented-control by default', () => {
    expect(component.cssClass).toBe('segmented-control');
  });

  it('should have css classes of "segmented-control foo" if we pass foo', () => {
    component.cssClass = 'foo';
    component.ngOnInit();
    expect(component.cssClass).toBe('segmented-control foo');
  });
});
