import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {
  navItems: Array<any>;

  ngOnInit() {
    this.navItems = [
      { name: 'Home', route: '/home' },
      { name: 'Styles', route: '/styles' },
      { name: 'Components', route: '/components' },
      { name: 'Patterns', route: '/patterns' }
    ];
  }
}
