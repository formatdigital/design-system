import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeViewComponent as HomeView } from './views/home/home.component';
import { StylesViewComponent as StylesView } from './views/styles/styles.component';
import { ComponentsViewComponent as ComponentsView } from './views/components/components.component';
import { PatternsComponent as PatternsView } from './views/patterns/patterns.component';
import { PatternsOverviewComponent as PatternsOverview } from './views/patterns/overview/overview.component';
import { PatternsFormComponent as FormView } from './views/patterns/form/form.component';
import { PatternsDateComponent as DateView } from './views/patterns/date/date.component';
import { PatternsDataComponent as DataView } from './views/patterns/data/data.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeView },
  { path: 'styles', component: StylesView },
  { path: 'components', component: ComponentsView },
  { path: 'patterns', component: PatternsView, children: [
    { path: '', component: PatternsOverview, outlet: 'patterns' },
    { path: 'form', component: FormView, outlet: 'patterns' },
    { path: 'date', component: DateView, outlet: 'patterns' },
    { path: 'data', component: DataView, outlet: 'patterns' }
  ] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    anchorScrolling: 'enabled',
    onSameUrlNavigation: 'reload',
    scrollPositionRestoration: 'enabled',
    scrollOffset: [0, 90]
  })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
