import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HomeViewComponent as HomeView } from './home.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('Home', () => {
  let component: HomeView;
  let fixture: ComponentFixture<HomeView>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeView],
      imports: [ RouterTestingModule ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeView);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create a home view', () => {
    expect(component).toBeTruthy();
  });
});
