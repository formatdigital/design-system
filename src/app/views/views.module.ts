import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';
import { ComponentsModule } from '../components/components.module';
import { TableModule } from 'primeng/table';
import { CalendarModule } from 'primeng/calendar';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CodeHighlighterModule } from 'primeng/codehighlighter';
import { DialogModule } from 'primeng/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HomeViewComponent as HomeView } from './home/home.component';
import { StylesViewComponent as StylesView } from './styles/styles.component';
import { ComponentsViewComponent as ComponentsView } from './components/components.component';
import { PatternsComponent as PatternsView } from './patterns/patterns.component';
import { PatternsOverviewComponent as PatternsOverview } from './patterns/overview/overview.component';
import { PatternsFormComponent as FormView } from './patterns/form/form.component';
import { PatternsDateComponent as DateView } from './patterns/date/date.component';
import { PatternsDataComponent as DataView } from './patterns/data/data.component';

@NgModule({
  declarations: [
    HomeView,
    StylesView,
    ComponentsView,
    PatternsView,
    PatternsOverview,
    FormView,
    DateView,
    DataView
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    TableModule,
    CalendarModule,
    AutoCompleteModule,
    CodeHighlighterModule,
    DialogModule
  ],
  exports: [
    HomeView,
    StylesView,
    ComponentsView,
    PatternsView,
    PatternsOverview
  ]
})

export class ViewsModule { }
