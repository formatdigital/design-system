import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-styles-view',
  templateUrl: './styles.component.html',
  styleUrls: ['./styles.component.scss']
})

export class StylesViewComponent implements OnInit {
  contextItems: Array<any>;
  fontHTML: string;
  headingHTML: string;

  ngOnInit() {
    this.fontHTML =
`<link
  href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
  rel="stylesheet">`;
    this.headingHTML =
`<h1 class="view-heading">A view heading</h1>
<h2 class="sub-heading">A sub heading</h2>
<h3 class="minor-heading">A minor heading</h3>`;

    this.contextItems = [
      { name: 'Colours', fragment: 'colours' },
      { name: 'Typography', fragment: 'typography' },
      { name: 'Icons', fragment: 'icons' },
      { name: 'Layout', fragment: 'layout' }
    ];
  }
}
