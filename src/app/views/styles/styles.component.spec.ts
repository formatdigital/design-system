import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { StylesViewComponent as StylesView } from './styles.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('Styles', () => {
  let component: StylesView;
  let fixture: ComponentFixture<StylesView>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StylesView],
      imports: [ RouterTestingModule ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StylesView);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create a styles view', () => {
    expect(component).toBeTruthy();
  });
});
