import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class CodeSnippetService {
  constructor() { }
  alertHTML =
`<app-alert>
  We will be offline for an hour at 03:30 CET tomorrow for essential maintenance.
</app-alert>

<app-alert status="warning">
  <i class="fas fa-exclamation-triangle"></i> This is an alert with a warning status.
</app-alert>`;
  autocompleteBasicHTML =
`<p-autoComplete [(ngModel)]="term"
  [suggestions]="results"
  (completeMethod)="search($event)"></p-autoComplete>`;
  autocompleteMultiHTML =
`<p-autoComplete [(ngModel)]="term"
  [suggestions]="results"
  (completeMethod)="search($event)"
  [multiple]="true"></p-autoComplete>`;
  autocompleteAdvancedHTML =
`<p-autoComplete [(ngModel)]="term"
  [suggestions]="results"
  (completeMethod)="search($event)"
  [multiple]="true"
  [dropdown]="true"></p-autoComplete>`;
  buttonHTML =
`<app-button (clicked)="clicked($event)">
  Button
</app-button>`;
  secondaryButtonHTML =
`<app-button cssClass="secondary" (clicked)="clicked($event)">
  Secondary
</app-button>`;
  primaryButtonHTML =
`<app-button type="submit" cssClass="primary" (clicked)="clicked($event)">
  Submit
</app-button>`;
  warningButtonHTML =
`<app-button cssClass="warning">
  Delete
</app-button>`;
  negativeButtonHTML =
`<app-button cssClass="negative">
  Cancel
</app-button>`;
  linkButtonHTML =
`<a routerLink="./" class="btn primary">Internal</a>
<a href="http://bbc.co.uk" class="btn warning">External</a>`;
  buttonJS = `clicked = event => console.log(event);`;
  calendarHTML =
`<fieldset class="form-fieldset">
  <legend class="form-legend">When did the sample arrive?</legend>
  <label class="form-label" for="deliveryDate">Delivery date</label>
  <p-calendar id="deliveryDate" name="deliveryDate"></p-calendar>
</fieldset>`;
  checkboxesHTML =
`<fieldset class="form-fieldset">
  <legend class="form-legend">Which studies do you want to see?</legend>
  <span class="form-hint">You can select more than one</span>
  <span class="form-checkbox-item">
    <input id="study-1" name="study-1" type="checkbox" value="study1" class="form-checkbox-input" />
    <label for="study-1" class="form-checkbox-label">
      <span class="form-checkbox">
        <i class="far fa-square"></i>
      </span>
      Study 1
    </label>
  </span>
  <span class="form-checkbox-item">
    <input id="study-2" name="study-2" type="checkbox" value="study2" class="form-checkbox-input" />
    <label for="study-2" class="form-checkbox-label">
      <span class="form-checkbox">
        <i class="far fa-square"></i>
      </span>
      Study 2
    </label>
  </span>
  <span class="form-checkbox-item">
    <input id="study-3" name="study-3" type="checkbox" value="study3" class="form-checkbox-input" />
    <label for="study-3" class="form-checkbox-label">
      <span class="form-checkbox">
        <i class="far fa-square"></i>
      </span>
      Study 3
    </label>
  </span>
</fieldset>`;
  dataTableHTML =
`<p-table [value]="studies" selectionMode="single" (onRowSelect)="selected($event)" [responsive]="true">
<ng-template pTemplate="caption">Select a study from the list</ng-template>
<ng-template pTemplate="header">
  <tr>
      <th>Name</th>
      <th>Code</th>
      <th>Status</th>
  </tr>
</ng-template>
<ng-template pTemplate="body" let-study>
  <tr [pSelectableRow]="study">
      <td class="table-header">{{study.name}}</td>
      <td>{{study.id}}</td>
      <td>{{study.status}}</td>
  </tr>
</ng-template>
</p-table>`;
  dataTableJS =
`this.studies = [
  { id: 'EF1', name: 'Study 1', status: 'On track' },
  { id: 'EF2', name: 'Study 2', status: 'On track' },
  { id: 'EF3', name: 'Study 3', status: 'On hold' }
];

selected = event => console.log(event);
`;
  dataTableMultiHTML =
`<p-table [value]="studies" selectionMode="" [(selection)]="selectedStudies" [responsive]="true">
  <ng-template pTemplate="caption">Select studies from the list</ng-template>
  <ng-template pTemplate="header">
    <tr>
        <th width="40px"></th>
        <th>Name</th>
        <th>Code</th>
        <th>Status</th>
    </tr>
  </ng-template>
  <ng-template pTemplate="body" let-study>
    <tr [pSelectableRow]="study" class="has-checkbox">
        <td><p-tableCheckbox [value]="study"></p-tableCheckbox></td>
        <td class="table-header">{{study.name}}</td>
        <td>{{study.id}}</td>
        <td>{{study.status}}</td>
    </tr>
  </ng-template>
</p-table>`;
  dateInputHTML =
`<fieldset class="form-fieldset">
  <legend class="form-legend">What is your date of birth?</legend>
  <span class="form-hint">For example, 26 11 1985</span>
  <app-date-input [formGroup]="dateForm"></app-date-input>
</fieldset>`;
  dateInputJS =
`this.dateForm = new FormGroup({
  day: new FormControl(),
  month: new FormControl(),
  year: new FormControl()
});`;
  dateInputErrorHTML =
`<fieldset class="form-fieldset form-error">
  <legend class="form-legend">What is your date of birth?</legend>
  <span class="form-hint">For example, 26 11 1985</span>
  <span class="form-error-message">The date must be in the past</span>
  <app-date-input cssClass="has-error"></app-date-input>
</fieldset>`;
  fieldsetHTML =
`<fieldset class="form-fieldset">
  <legend class="form-legend">What is the address?</legend>
  <span class="form-hint">You can give the user a hint, if you think it will help</span>
  <div class="form-group">
    <label for="address-line-1" class="form-label">Building name and street</label>
    <input id="address-line-1" name="address-line-1" type="text" class="form-input" />
    <input id="address-line-2" name="address-line-2" type="text" class="form-input" />
  </div>
  <label for="address-line-3" class="form-label">Town or city</label>
  <input id="address-line-3" name="address-line-3" type="text" class="form-input width-two-thirds" />
  <label for="address-line-4" class="form-label">County</label>
  <input id="address-line-4" name="address-line-4" type="text" class="form-input width-two-thirds" />
  <label for="postcode" class="form-label">Postcode</label>
  <input id="postcode" name="postcode" type="text" class="form-input width-one-third" placeholder="Required" />
</fieldset>`;
  fieldsetErrorHTML =
`<fieldset class="form-fieldset form-error">
  <legend class="form-legend">What is the address?</legend>
  <span class="form-error-message">You must enter a valid postcode</span>
  <span class="form-hint">You can give the user a hint, if you think it will help</span>
  <div class="form-group">
    <label for="address-line-1" class="form-label">Building name and street</label>
    <input id="address-line-1" name="address-line-1" type="text" class="form-input" />
    <input id="address-line-2" name="address-line-2" type="text" class="form-input" />
  </div>
  <label for="address-line-3" class="form-label">Town or city</label>
  <input id="address-line-3" name="address-line-3" type="text" class="form-input width-two-thirds" />
  <label for="address-line-4" class="form-label">County</label>
  <input id="address-line-4" name="address-line-4" type="text" class="form-input width-two-thirds" />
  <label for="postcode" class="form-label">Postcode</label>
  <span class="form-validation">A postcode is required.</span>
  <input id="postcode" name="postcode" type="text" class="form-input width-one-third has-error" placeholder="Required" />
</fieldset>`;
  footerHTML = `<app-footer></app-footer>`;
  headerHTML =
`<app-header [sticky]="false" [navItems]="navItems">
  <img src="assets/images/lumentum-logo.svg" alt="Lumentum" class="app-logo" />
</app-header>`;
  headerJS =
`this.navItems = [
  { name: 'Home', route: './' },
  { name: 'Contact', route: '/contact' }
]`;
  loadingHTML = `<app-loading-indicator [loading]="true"></app-loading-indicator>`;
  phaseBannerHTML =
`<app-phase-banner
  phase="Beta"
  feedbackUrl="https://www.surveymonkey.com">
</app-phase-banner>`;
  radioHTML =
`<fieldset class="form-fieldset">
  <legend class="form-legend">Have you completed it?</legend>
  <span class="form-radio-item inline">
    <input id="completed" name="completed" type="radio" value="yes" class="form-radio-input" checked />
    <label for="completed" class="form-radio-label">
      <span class="form-radio">
        <i class="far fa-circle"></i>
      </span>
      Yes
    </label>
  </span>
  <span class="form-radio-item inline">
    <input id="completed-1" name="completed" type="radio" value="no" class="form-radio-input" />
    <label for="completed-1" class="form-radio-label">
      <span class="form-radio">
        <i class="far fa-circle"></i>
      </span>
      No
    </label>
  </span>
</fieldset>`;
  segmentedHTML =
`<app-segmented-control *ngIf="segments"
  name="view-types"
  [segments]="segments"
  (changed)="changed($event)">
</app-segmented-control>`;
  segmentedJS =
`this.segments = [
  { id: 'bar-chart', value: 'bar', label: 'Bar chart', checked: true },
  { id: 'line-chart', value: 'line', label: 'Line chart' },
  { id: 'table-view', value: 'table', label: 'Table' }
]

changed = event => console.log(event);`;
  selectHTML =
`<label class="form-label" for="sort">Sort by</label>
<select id="sort" name="sort" class="form-select width-half">
  <option value="published">Recently published</option>
  <option value="updated" selected>Recently updated</option>
  <option value="views">Most views</option>
  <option value="comments">Most comments</option>
</select>`;
  tableHTML =
`<table class="table">
  <caption class="table-caption">Budget for studies</caption>
  <thead>
    <tr>
      <th scope="col" class="table-header width-half">Study</th>
      <th scope="col" class="table-header width-one-quarter">Status</th>
      <th scope="col" class="table-header numeric width-one-quarter">Amount</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row" class="table-header">Study 1</th>
      <td class="table-cell">On track</td>
      <td class="table-cell numeric">£10,000</td>
    </tr>
    <tr>
      <th scope="row" class="table-header">Study 2</th>
      <td class="table-cell">On track</td>
      <td class="table-cell numeric">£5,000</td>
    </tr>
    <tr>
      <th scope="row" class="table-header" colspan="2">Total</th>
      <td class="table-cell numeric">£15,000</td>
    </tr>
  </tbody>
</table>`;
  tabsHTML =
`<app-tabs>
  <app-tab title="Studies">
    <h2 class="sub-heading">A list of studies</h2>
  </app-tab>
  <app-tab title="Sites">
    <h2 class="sub-heading">A list of sites</h2>
  </app-tab>
</app-tabs>`;
  textInputHTML =
`<label for="site-name" class="form-label">Site name</label>
<input id="site-name" name="site-name" type="text" class="form-input" />`;
  textInputWidthHTML =
`<label for="telephone" class="form-label">Telephone number</label>
<input id="telephone" name="telephone" type="text" class="form-input width-half" />`;
  textareaHTML =
`<label for="more-detail" class="form-label">Please provide more information</label>
<textarea id="more-detail" name="more-detail" class="form-textarea" rows="5"></textarea>`;
}
