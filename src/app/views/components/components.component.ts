import { Component, OnInit } from '@angular/core';
import { CodeSnippetService } from './code-snippet.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-home-view',
  templateUrl: './components.component.html',
  styleUrls: ['./components.component.scss']
})

export class ComponentsViewComponent implements OnInit {
  constructor(public codeSnippets: CodeSnippetService) {}
  contextItems: Array<object>;
  navItems: Array<object>;
  segments: Array<object>;
  studies: Array<object>;
  selectedStudies: Array<object>;
  term: string;
  searchList: Array<string>;
  results: Array<string>;
  autocompleteSegments: Array<object>;
  activeSegment: string;
  dateForm: FormGroup;

  clicked = event => console.log(event);
  changed = event => console.log(event);
  selected = event => console.log(event);
  search(event) {
    this.results = this.searchList.filter(result => result.toLowerCase().includes(event.query.toLowerCase()));
  }
  autocompleteChanged = event => {
    this.activeSegment = event;
    this.term = '';
  }

  ngOnInit() {
    this.activeSegment = 'single';
    this.searchList = ['Study 1', 'Study 2', 'Study 3', 'Study 4', 'Study 5'];
    this.navItems = [
      { name: 'Home', route: './' },
      { name: 'Contact', route: '/contact' }
    ];

    this.dateForm = new FormGroup({
      day: new FormControl(),
      month: new FormControl(),
      year: new FormControl()
    });

    this.segments = [
      { id: 'bar-chart', name: 'view-types', value: 'bar', label: 'Bar chart', checked: true },
      { id: 'line-chart', name: 'view-types', value: 'line', label: 'Line chart' },
      { id: 'table-view', name: 'view-types', value: 'table', label: 'Table' }
    ];

    this.autocompleteSegments = [
      { id: 'single', name: 'autocomplete-types', value: 'single', label: 'Basic', checked: true },
      { id: 'multiple', name: 'autocomplete-types', value: 'multiple', label: 'Multiple' },
      { id: 'advanced', name: 'autocomplete-types', value: 'advanced', label: 'Select' }
    ];

    this.studies = [
      { id: 'EF1', name: 'Study 1', status: 'On track' },
      { id: 'EF2', name: 'Study 2', status: 'On track' },
      { id: 'EF3', name: 'Study 3', status: 'On hold' }
    ];

    this.contextItems = [
      { name: 'Alert', fragment: 'alert' },
      { name: 'Auto complete', fragment: 'auto-complete' },
      { name: 'Button', fragment: 'button' },
      { name: 'Calendar', fragment: 'calendar' },
      { name: 'Checkboxes', fragment: 'checkboxes' },
      { name: 'Data table', fragment: 'data-table' },
      { name: 'Date input', fragment: 'date-input' },
      { name: 'Fieldset', fragment: 'fieldset' },
      { name: 'Footer', fragment: 'footer' },
      { name: 'Header', fragment: 'header' },
      { name: 'Loading indicator', fragment: 'loading-indicator'},
      { name: 'Phase banner', fragment: 'phase-banner' },
      { name: 'Radios', fragment: 'radios' },
      { name: 'Segmented control', fragment: 'segmented-control' },
      { name: 'Select', fragment: 'select' },
      { name: 'Table', fragment: 'table' },
      { name: 'Tabs', fragment: 'tabs' },
      { name: 'Text input', fragment: 'text-input' },
      { name: 'Textarea', fragment: 'textarea' }
    ];
  }
}
