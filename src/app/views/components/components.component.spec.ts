import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ComponentsViewComponent as ComponentsView } from './components.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('Components', () => {
  let component: ComponentsView;
  let fixture: ComponentFixture<ComponentsView>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ComponentsView],
      imports: [RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentsView);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create a components view', () => {
    expect(component).toBeTruthy();
  });
});
