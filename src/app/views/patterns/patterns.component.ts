import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-patterns',
  templateUrl: './patterns.component.html',
  styleUrls: ['./patterns.component.scss']
})
export class PatternsComponent implements OnInit {
  contextItems: Array<object>;
  ngOnInit() {
    this.contextItems = [
      { name: 'Asking questions', route: 'form' },
      { name: 'Date input', route: 'date' },
      { name: 'Managing data', route: 'data' }
    ];
  }
}
