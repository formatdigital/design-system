import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormHelperService } from '../formHelper.service';
import { CodeSnippetService } from './code-snippet.service';

@Component({
  selector: 'app-patterns-form',
  templateUrl: './form.component.html',
  styleUrls: ['../patterns.component.scss']
})

export class PatternsFormComponent implements OnInit {
  constructor(
    private formHelper: FormHelperService,
    public codeSnippets: CodeSnippetService) {}

  form: FormGroup;
  result: any;
  hasErrors: boolean;

  get addressLine1() { return this.form.get('addressLine1'); }
  get city() { return this.form.get('city'); }
  get county() { return this.form.get('county'); }
  get postcode() { return this.form.get('postcode'); }

  onSubmit(data) {
    if (this.form.valid) {
      this.hasErrors = false;
      this.result = data;
    } else {
      this.hasErrors = true;
      this.form.markAsTouched();
      this.formHelper.validateAllFormFields(this.form);
    }
  }

  clearForm() {
    this.form.reset({ site: true });
    this.hasErrors = false;
  }

  ngOnInit() {
    this.form = new FormGroup({
      site: new FormControl(true, Validators.required),
      addressLine1: new FormControl('', Validators.required),
      addressLine2: new FormControl(''),
      city: new FormControl('', Validators.required),
      county: new FormControl('', Validators.required),
      postcode: new FormControl('', Validators.required)
    });
  }
}
