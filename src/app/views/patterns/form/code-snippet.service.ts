import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class CodeSnippetService {
  constructor() { }
  formHTML =
`<form [formGroup]="form" (ngSubmit)="onSubmit(form.value)" [ngClass]="hasErrors === true ? 'form-error' : ''">
<legend class="form-legend">What is the address?</legend>
<span *ngIf="hasErrors === true" class="form-error-message">This form has errors and has not been saved.</span>
<span class="form-hint">This is the address that will be associated with the study.</span>
<fieldset class="form-fieldset">
  <legend class="form-legend">Is this a site address?</legend>
  <span class="form-radio-item inline">
  <input id="yes" name="site" type="radio" [value]="true" class="form-radio-input" formControlName="site" />
  <label for="yes" class="form-radio-label">
  <span class="form-radio"><i class="far fa-circle"></i></span>Yes
  </label>
  </span>
  <span class="form-radio-item inline">
  <input id="no" name="site" type="radio" [value]="false" class="form-radio-input" formControlName="site" />
  <label for="no" class="form-radio-label">
  <span class="form-radio"><i class="far fa-circle"></i></span>No
  </label>
  </span>
</fieldset>
<fieldset class="form-fieldset">
  <div class="form-group">
    <label for="address-line-1" class="form-label">Building name and street</label>
    <span *ngIf="addressLine1.invalid && (addressLine1.dirty || addressLine1.touched)" class="form-validation">
      We need the first line of your address.</span>
    <input id="address-line-1" name="address-line-1" type="text" class="form-input" formControlName="addressLine1" placeholder="Required" />
    <input id="address-line-2" name="address-line-2" type="text" class="form-input" formControlName="addressLine2" />
  </div>
  <label for="address-line-3" class="form-label">Town or city</label>
  <span *ngIf="city.invalid && (city.dirty || city.touched)" class="form-validation">
    A town or city is required.</span>
  <input id="address-line-3" name="address-line-3" type="text" class="form-input width-two-thirds"
    formControlName="city" placeholder="Required" />
  <label for="address-line-4" class="form-label">County</label>
  <span *ngIf="county.invalid && (county.dirty || county.touched)" class="form-validation">
    A county is required.</span>
  <input id="address-line-4" name="address-line-4" type="text" class="form-input width-two-thirds"
    formControlName="county" placeholder="Required" />
  <label for="postcode" class="form-label">Postcode</label>
  <span *ngIf="postcode.invalid && (postcode.dirty || postcode.touched)" class="form-validation">
    A postcode is required.</span>
  <input id="postcode" name="postcode" type="text" class="form-input width-one-third" formControlName="postcode" placeholder="Required" />
</fieldset>
<app-button type="submit" cssClass="primary">Save</app-button>
<app-button cssClass="negative" (click)="clearForm()">Clear</app-button>
</form>`;
  initJS =
`ngOnInit() {
  this.form = new FormGroup({
    site: new FormControl(true, Validators.required),
    addressLine1: new FormControl('', Validators.required),
    addressLine2: new FormControl(''),
    city: new FormControl('', Validators.required),
    county: new FormControl('', Validators.required),
    postcode: new FormControl('', Validators.required)
  });
}`;
  submitJS =
`onSubmit(data) {
  if (this.form.valid) {
    this.hasErrors = false;
    this.result = data;
  } else {
    this.hasErrors = true;
    this.form.markAsTouched();
    this.formHelper.validateAllFormFields(this.form);
  }
}`;
}
