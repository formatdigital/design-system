import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormHelperService } from '../formHelper.service';
import { CodeSnippetService } from './code-snippet.service';
import * as moment from 'moment';

@Component({
  selector: 'app-patterns-date',
  templateUrl: './date.component.html',
  styleUrls: ['../patterns.component.scss']
})

export class PatternsDateComponent implements OnInit {
  constructor(
    private formHelper: FormHelperService,
    public codeSnippets: CodeSnippetService) {}

  dateSegments: Array<object>;
  activeSegment: string;
  form: FormGroup;
  sampleForm: FormGroup;
  result: string;
  hasErrors: boolean;
  validation: string;

  get date() { return this.form.get('date'); }
  dateTypeChanged = event => {
    this.activeSegment = event;
    return this.activeSegment;
  }

  onSubmit(data) {
    if (this.form.valid || this.sampleForm.valid) {
      this.hasErrors = false;
      this.result = this.formatDate(data);
      return this.result;
    } else {
      this.throwValidation();
    }
  }

  clearForm() {
    this.form.reset();
    this.sampleForm.reset();
    this.hasErrors = false;
    return true;
  }

  throwValidation(message = 'A valid date is required') {
    this.hasErrors = true;
    this.form.markAsTouched();
    this.formHelper.validateAllFormFields(this.form);
    this.formHelper.validateAllFormFields(this.sampleForm);
    this.validation = message;
    return false;
  }

  formatDate(input) {
    const date = input.year ? new Date(`${input.year}/${input.month}/${input.day}`) : input.sampleDate;
    this.validateDate(date);
    return moment(date).format('DDMMMYYYY');
  }

  validateDate(date) {
    const isValid = date instanceof Date && !isNaN(Number(date));
    if (isValid) {
      const today = new Date();
      return date > today ? this.throwValidation('The date must be in the past') : true;
    } else {
      this.throwValidation();
    }
  }

  ngOnInit() {
    this.form = new FormGroup({
      day: new FormControl('', Validators.required),
      month: new FormControl('', Validators.required),
      year: new FormControl('', Validators.required)
    });

    this.sampleForm = new FormGroup({
      sampleDate: new FormControl('', Validators.required),
    });

    this.activeSegment = 'calendar';
    this.dateSegments = [
      { id: 'calendar', name: 'date-types', value: 'calendar', label: 'Calendar', checked: true },
      { id: 'input', name: 'date-types', value: 'input', label: 'Date input' }
    ];
  }
}
