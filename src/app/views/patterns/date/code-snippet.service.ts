import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class CodeSnippetService {
  constructor() { }
  formHTML =
`<form [formGroup]="form" (ngSubmit)="onSubmit(form.value)" [ngClass]="hasErrors === true ? 'form-error' : ''">
  <legend class="form-legend">What is your date of birth?</legend>
  <span *ngIf="hasErrors === true" class="form-error-message">This form has errors and has not been saved.</span>
  <span class="form-hint">For example, 26 11 1985</span>
  <app-date-input [formGroup]="form"></app-date-input>
  <span *ngIf="hasErrors === true" class="form-validation">{{validation}}</span>
  <app-button type="submit" cssClass="primary">Save</app-button>
  <app-button cssClass="negative" (click)="clearForm()">Clear</app-button>
</form>`;
  sampleFormHTML =
`<form [formGroup]="form" (ngSubmit)="onSubmit(form.value)" [ngClass]="hasErrors === true ? 'form-error' : ''">
  <legend class="form-legend">When was the sample sent?</legend>
  <span *ngIf="hasErrors === true" class="form-error-message">This form has errors and has not been saved.</span>
  <div class="form-group">
    <label class="form-label" for="sampleDate">Date</label>
    <p-calendar
      id="sampleDate"
      name="sampleDate"
      formControlName="sampleDate"
      hideOnDateTimeSelect="true"
      dateFormat="ddmmy"></p-calendar>
    <span *ngIf="hasErrors === true" class="form-validation">{{validation}}</span>
  </div>
  <app-button type="submit" cssClass="primary">Save</app-button>
  <app-button cssClass="negative" (click)="clearForm()">Clear</app-button>
</form>`;
  initJS =
`ngOnInit() {
  this.form = new FormGroup({
    day: new FormControl('', Validators.required),
    month: new FormControl('', Validators.required),
    year: new FormControl('', Validators.required)
  });
}`;
  sampleInitJS =
`ngOnInit() {
  this.form = new FormGroup({
    sampleDate: new FormControl('', Validators.required),
  });
}`;
  submitJS =
`onSubmit(data) {
  if (this.form.valid) {
    this.result = this.formatDate(data);
    return this.result;
  } else {
    this.throwValidation();
  }
}

throwValidation(message = 'A valid date is required') {
  this.hasErrors = true;
  this.form.markAsTouched();
  this.formHelper.validateAllFormFields(this.form);
  this.validation = message;
  return false;
}`;
validationJS =
`formatDate(input) {
  const date = new Date(input);
  this.validateDate(date);
  return moment(date).format('DDMMMYYYY');
}

validateDate(date) {
  const isValid = date instanceof Date && !isNaN(Number(date));
  if (isValid) {
    const today = new Date();
    return date > today ? this.throwValidation('The date must be in the past') : true;
  } else {
    this.throwValidation();
  }
}`;
  sampleValidationJS =
`formatDate(input) {
  this.validateDate(input.sampleDate);
  return moment(input).format('DDMMMYYYY');
}

validateDate(date) {
  const isValid = date instanceof Date && !isNaN(Number(date));
  if (isValid) {
    const today = new Date();
    return date > today ? this.throwValidation('The date must be in the past') : true;
  } else {
    this.throwValidation();
  }
}`;
}
