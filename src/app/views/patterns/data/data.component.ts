import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormGroupDirective } from '@angular/forms';
import { CodeSnippetService } from './code-snippet.service';

@Component({
  selector: 'app-patterns-data',
  templateUrl: './data.component.html',
  styleUrls: ['../patterns.component.scss']
})

export class PatternsDataComponent implements OnInit {
  constructor(public codeSnippets: CodeSnippetService) {}
  display = false;
  readMode = true;
  form: FormGroup;
  studies: Array<object>;
  selectedRow: any;
  formEdit: FormGroupDirective;

  get name() { return this.form.get('name'); }
  get description() { return this.form.get('description'); }

  selected = event => {
    this.display = true;
    this.readMode = true;
    this.selectedRow = event.data;
  }

  close = () => this.display = false;
  edit = () => this.readMode = false;
  save = data => {
    if (data.name) {
      this.selectedRow.name = data.name;
    }
    if (data.description) {
      this.selectedRow.description = data.description;
    }
    this.readMode = true;
  }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(),
      description: new FormControl()
    });

    this.studies = [
      { id: 'EF1', name: 'Study 1', status: 'On track', description: 'An explanation of the status of study 1.' },
      { id: 'EF2', name: 'Study 2', status: 'On track', description: 'An explanation of the status of study 2.' },
      { id: 'EF3', name: 'Study 3', status: 'On hold', description: 'An explanation of the status of study 3.' },
      { id: 'EF4', name: 'Study 4', status: 'On track', description: 'An explanation of the status of study 4.' },
      { id: 'EF5', name: 'Study 5', status: 'On track', description: 'An explanation of the status of study 5.' },
      { id: 'EF6', name: 'Study 6', status: 'On hold', description: 'An explanation of the status of study 6.' },
      { id: 'EF7', name: 'Study 7', status: 'On track', description: 'An explanation of the status of study 7.' },
      { id: 'EF8', name: 'Study 8', status: 'On track', description: 'An explanation of the status of study 8.' },
      { id: 'EF9', name: 'Study 9', status: 'On hold', description: 'An explanation of the status of study 9.' },
      { id: 'EF10', name: 'Study 10', status: 'On hold', description: 'An explanation of the status of study 10.' }
    ];
  }
}
