import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class CodeSnippetService {
  constructor() { }
  dataTableHTML =
`<p-table
  [value]="studies"
  selectionMode="single"
  (onRowSelect)="selected($event)"
  [responsive]="true"
  scrollable]="true"
  scrollHeight="180px">
<ng-template pTemplate="caption">Select a study from the list</ng-template>
<ng-template pTemplate="header">
  <tr>
      <th>Name</th>
      <th>Code</th>
      <th>Status</th>
  </tr>
</ng-template>
<ng-template pTemplate="body" let-study>
  <tr [pSelectableRow]="study">
      <td class="table-header">{{study.name}}</td>
      <td>{{study.id}}</td>
      <td>{{study.status}}</td>
  </tr>
</ng-template>
</p-table>`;
  dialogHTML =
`<p-dialog
  [header]="selectedRow?.name"
  [(visible)]="display"
  [modal]="true"
  styleClass="modal">
<p><b>{{ selectedRow?.id }}</b> is <em>{{ selectedRow?.status }}</em></p>
<h3 class="minor-heading">More details</h3>
<p *ngIf="readMode; else editMode">{{ selectedRow?.description }}</p>
<form [formGroup]="form" (ngSubmit)="save(form.value)" #formEdit="ngForm">
  <ng-template #editMode>
    <label for="name" class="form-label">Name</label>
    <input id="name" name="name" class="form-input" formControlName="name" [value]="selectedRow?.name" />
    <textarea
    id="description"
    name="description"
    class="form-textarea"
    rows="5"
    [value]="selectedRow?.description"
    formControlName="description"></textarea>
  </ng-template>
</form>
<p-footer>
  <app-button cssClass="negative" (clicked)="close()">Cancel</app-button>
  <app-button *ngIf="readMode" (clicked)="edit()">Edit</app-button>
  <app-button *ngIf="!readMode" type="submit" cssClass="primary" (click)="formEdit.ngSubmit.emit()">Save</app-button>
</p-footer>
</p-dialog>`;

  dialogJS =
`selected = event => {
  this.display = true;
  this.readMode = true;
  this.selectedRow = event.data;
}

close = () => this.display = false;
edit = () => this.readMode = false;
save = data => {
  if (data.name) {
    this.selectedRow.name = data.name;
  }
  if (data.description) {
    this.selectedRow.description = data.description;
  }
  this.readMode = true;
}`;
}
